// Create a trainer object using object literals
let trainer = {};

// Initialize/add the following trainer object properties:

// Name (String)
trainer.name = "Ash Ketchum";

// Age (Number)
trainer.age = 10;

// Pokemon (Array)
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];

// Friends (Object with Array values for properties)
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
};

console.log(trainer);

// Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
trainer.talk = function(){
    console.log("Pikachu! I choose you!");
};

// Access the trainer object properties using dot and square bracket notation
console.log("Result of dot notation:");
console.log(trainer.pokemon[0]);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Invoke/call the trainer talk object method
trainer.talk();

// Create a constructor for creating a pokemon with the following properties:

function Pokemon(name, level){
    // Name (Provided as an argument to the constructor)
    this.name = name;
    // Level (Provided as an argument to the constructor)
    this.level = level;
    // Health (Create an equation that uses the level property)
    this.health = level * 2;
    // Attack (Create an equation that uses the level property)
    this.attack = level;

    // Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
    this.tackle = function(target){
    console.log(this.name+ " tackled " +target.name);

    console.log(target.name+ "'s health is reduced to " + (target.health - this.attack));

    // Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
    if (target.health - this.attack <= 0){

        target.faint(target);
        }

    };

    // Create a faint method that will print out a message of targetPokemon has fainted.
    this.faint = function(target){
    console.log(target.name+ " has fainted.");
    }

}

// Create/instantiate several pokemon objects from the constructor with varying name and level properties.
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);

mewtwo.tackle(geodude);